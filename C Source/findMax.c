#include "assignment3.h"

double findMax(double **rows, int ncolumns, int nrows, int my_row) {
   double max = 0, sum;
   int i, j;
   for (i = 0; i < nrows; i++) {
      sum = 0;
      for (j = 0; j < ncolumns; j++) 
         sum += rows[i][j] > 0? rows[i][j] : -rows[i][j];
      if (sum > max) max = sum;
   }
   return max;
}
