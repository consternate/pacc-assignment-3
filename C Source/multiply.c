#include "assignment3.h"

void *multiply(void *arg){
   thread_data_t *data = arg;
   int n = data->n, 
       p = data->p, 
       my_row = (*data).my_row;
   cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n/p, n, n, 1.0,
         (data->A)[my_row], n, (data->B)[0], n, 0.0, (data->C)[my_row], n);

   pthread_mutex_lock(data->mutex);
   double max = findMax(data->C + my_row, n, n/p, data->my_row);
   if (*(data->global_norm) < max) 
      *(data->global_norm) = max;
   pthread_mutex_unlock(data->mutex);

   pthread_exit(NULL);
   return NULL;
}
