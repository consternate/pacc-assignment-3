#include <cblas.h>
#include "assignment3.h"

double multiply_serial(double **A, double **B, double **C, int n) {
   cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1.0,
         A[0], n, B[0], n, 0.0, C[0], n);
   return findMax(C, n, n, 0);
}

