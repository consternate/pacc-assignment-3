#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include "assignment3.h"

int main(int argc, const char *argv[])
{
   int n, p = 1;
   printf("Please enter the number of processors.\n > ");
   scanf("%d", &p);
   
   double **A, **B, **C;
   int i;
   double elapsed, elapsed_serial;
   double global_norm, global_norm_serial;
   pthread_t *workers;
   thread_data_t *data;
   pthread_mutex_t mutex;
   void **status = malloc(sizeof(void *)); // leaving out malloc is fine on osx, not on linux
   struct timeval tv1, tv2;
   struct timezone tz;
   FILE *parallel_times = fopen("outputs/parallel.txt", "w+"), 
        *serial_times = fopen("outputs/serial.txt", "w+");


   for (n = p; n <= 4096; n *= 2) {
      printf("Beginning n = %d\n", n);

      A = malloc(n*sizeof(double *));
      B = malloc(n*sizeof(double *));
      C = malloc(n*sizeof(double *));
      if (A == NULL || B == NULL || C == NULL) return 1;
      A[0] = malloc(n*n*sizeof(double));
      B[0] = malloc(n*n*sizeof(double));
      C[0] = calloc(n*n, sizeof(double));
      for (i = 1; i < n; i++) {
         A[i] = A[0] + i*n;
         B[i] = B[0] + i*n;
         C[i] = C[0] + i*n;
      }

      fillMatrix(n, A);
      fillMatrix(n, B);

      workers = malloc(p * sizeof(pthread_t));
      data = malloc(p * sizeof(thread_data_t));
      pthread_mutex_init(&mutex, NULL);

      for (i = 0; i < p; i++) {
         data[i].n = n;
         data[i].p = p;
         data[i].my_row = i * n/p;
         data[i].A = A;
         data[i].B = B;
         data[i].C = C;
         data[i].mutex = &mutex;
         data[i].global_norm = &global_norm;
      }

      gettimeofday(&tv1, &tz);
      for (i = 0; i < p; i++) 
         pthread_create(&workers[i], NULL, multiply, &data[i]);

      for (i = 0; i < p; i++) pthread_join(workers[i], status);
      gettimeofday(&tv2, &tz);
      elapsed = (double) (tv2.tv_sec - tv1.tv_sec) + (double)
         (tv2.tv_usec - tv1.tv_usec) * 1.e-6;
      fprintf(parallel_times, "%3.10lf ", elapsed);

      // do non-parallel computation
      gettimeofday(&tv1, &tz);
      global_norm_serial = multiply_serial(A, B, C, n);
      gettimeofday(&tv2, &tz);
      elapsed_serial = (double) (tv2.tv_sec - tv1.tv_sec) + (double)
         (tv2.tv_usec - tv1.tv_usec) * 1.e-6;
      fprintf(serial_times, "%3.10lf ", elapsed_serial);

      /* printMatrix(n, A); */
      /* puts("Matrix B:"); */
      /* printMatrix(n, B); */
      /* puts("Matrix C:"); */
      /* printMatrix(n, C); */

      free(A[0]);
      free(B[0]);
      free(C[0]);
      free(A);
      free(B);
      free(C);
      free(workers);
      free(data);
   }

   fclose(parallel_times);
   fclose(serial_times);
   pthread_mutex_destroy(&mutex);
   return 0;
}
