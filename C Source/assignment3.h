#include <pthread.h>
#include <cblas.h>

typedef struct {
   int id;
   int n, p, my_row;
   double **A;
   double **B;
   double **C;
   double *global_norm;
   pthread_mutex_t *mutex;
} thread_data_t;

double findMax(double **rows, int ncolumns, int nrows, int my_row);
double multiply_serial(double **A, double **B, double **C, int n);
void *multiply(void *arg);
void printMatrix(int size, double** a);
void fillMatrix(int size, double** mtrix);
