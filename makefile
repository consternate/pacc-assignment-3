CFILES=C\ Source/fillMatrix.c C\ Source/multiply.c C\ Source/printMatrix.c C\ Source/findMax.c C\ Source/multiply_serial.c
MANUALPROG=C\ Source/assignment3.c
AUTOPROG=C\ Source/assignment3_auto.c

all:
	gcc-mp-4.7 -I/opt/local/include -L/opt/local/lib $(CFILES) $(MANUALPROG) -lcblas -latlas -Wall -o assignment3

auto:
	gcc-mp-4.7  -I/opt/local/include -L/opt/local/lib $(CFILES) $(AUTOPROG) -lcblas -latlas -Wall -o assignment3


# all:
# 	clang -O4 -o assignment3 -I/opt/local/include -L/opt/local/lib $(CFILES) $(MANUALPROG) -lcblas -latlas

# auto:
# 	clang -O4 -o assignment3_auto -I/opt/local/include -L/opt/local/lib $(CFILES) $(AUTOPROG) -lcblas -latlas
