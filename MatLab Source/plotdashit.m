clear;clc;close all;

parallel_times = load('../outputs/parallel.txt');
serial_times = load('../outputs/serial.txt');

% matrix sizes
ns = 2.^(0:13);
ns = ns(ns >= 16);

colors = jet(length(ns)); % using this as the color argument to scatter 
                          % unbelievably fucks up the whole rendering of 
                          % the figure

%%
f = figure;
scatter(ns, parallel_times, 500, 'Marker', '+', 'MarkerEdgeColor', 'red'); 
title('Runtime versus matrix size for 16 threads');
xlabel('Matrix size');
ylabel('Runtime in sec');
save2pdf('../Plots/fig1');
set(gca, 'XLim', [0,1100]);
save2pdf('../Plots/fig1_zoomin');

%%
f2 = figure;
scatter(ns, serial_times ./ parallel_times, 500, 'Marker', '+', 'MarkerEdgeColor', 'red');
title('Speedup of multithreaded computation versus serial execution');
xlabel('Matrix size');
ylabel('Speedup factor');
save2pdf('../Plots/fig2');
% result: speedup greater 1 for sizes larger than 256, before that the overhead
% kills the speedup