#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include "assignment3.h"

int main(int argc, const char *argv[])
{
   int n, p = 1;
   printf("Please enter the number of processors.\n > ");
   scanf("%d", &p);
   do {
      printf("Please enter the matrix factor "
            "(matrix size is a multiple of processor number).\n > ");
         scanf("%d", &n);
   } while(n % p);
   /* printf("n and p have values %d and %d\n", n, p); */
   
   // make matrices
   double **A, **B, **C;
   A = malloc(n*sizeof(double *));
   B = malloc(n*sizeof(double *));
   C = malloc(n*sizeof(double *));
   if (A == NULL || B == NULL || C == NULL) {
      return 1;
   }
   A[0] = malloc(n*n*sizeof(double));
   B[0] = malloc(n*n*sizeof(double));
   C[0] = calloc(n*n, sizeof(double));
   int i;
   for (i = 1; i < n; i++) {
      A[i] = A[0] + i*n;
      B[i] = B[0] + i*n;
      C[i] = C[0] + i*n;
   }

   fillMatrix(n, A);
   fillMatrix(n, B);

   pthread_t *workers = malloc(p * sizeof(pthread_t));
   thread_data_t *data = malloc(p * sizeof(thread_data_t));
   pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
   double global_norm;
   void **status = malloc(sizeof(void *)); // leaving out malloc is fine on osx, not on linux

   for (i = 0; i < p; i++) {
      data[i].n = n;
      data[i].p = p;
      data[i].my_row = i * n/p;
      data[i].A = A;
      data[i].B = B;
      data[i].C = C;
      data[i].mutex = &mutex;
      data[i].global_norm = &global_norm;
   }

   struct timeval tv1, tv2;
   struct timezone tz;

   gettimeofday(&tv1, &tz);
   for (i = 0; i < p; i++) 
      pthread_create(&workers[i], NULL, multiply, &data[i]);

   for (i = 0; i < p; i++) pthread_join(workers[i], status);
   gettimeofday(&tv2, &tz);
   double elapsed = (double) (tv2.tv_sec - tv1.tv_sec) + (double)
      (tv2.tv_usec - tv1.tv_usec) * 1.e-6;

   // do non-parallel computation
   struct timeval tv3, tv4;
   struct timezone tz2;
   gettimeofday(&tv3, &tz2);
   double global_norm_serial = multiply_serial(A, B, C, n);
   gettimeofday(&tv4, &tz2);
   double elapsed_serial = (double) (tv4.tv_sec - tv3.tv_sec) + (double)
      (tv4.tv_usec - tv3.tv_usec) * 1.e-6;

   printf("Time elapsed for parallel execution: %lf seconds.\n", elapsed);
   printf("Time elapsed for serial execution: %lf seconds.\n", elapsed_serial);



   /* puts("Matrix A:"); */
   /* printMatrix(n, A); */
   /* puts("Matrix B:"); */
   /* printMatrix(n, B); */
   /* puts("Matrix C:"); */
   /* printMatrix(n, C); */
   printf("The maximum row sum norm is %lf.\n", global_norm);
   printf("The maximum row sum norm is %lf.\n", global_norm_serial);

   free(A[0]);
   free(B[0]);
   free(C[0]);
   free(A);
   free(B);
   free(C);
   free(workers);
   free(data);
   pthread_mutex_destroy(&mutex);
   return 0;
}
